import React, { Component } from "react";
import "./calculator.css"

class ButtonsSet extends Component {

  render() {
    return (
      <div className="buttonSet">
        {this.props.onButtons.map((button) => {
          return (
            <button
              onClick={()=>this.props.onCalculate(button.value)}
              key={button.value}
              className="btn btn-primary m-2"
            >
              {button.value}
            </button>
          );
        })}
      </div>
    );
  }
}
export default ButtonsSet;
