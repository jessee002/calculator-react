import React, { Component } from "react";
import ButtonsSet from "./buttonsSet";
import CalculationBar from "./calculationBar";
import "./calculator.css";

class Calculator extends Component {
  state = {
    valueCalculate: "",
    buttons: [
      {value: 'clear'},
      { value: "+" },
      { value: "=" },
      { value: "/" },
      { value: "-" },
      { value: "*" },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 },
      { value: 5 },
      { value: 6 },
      { value: 7 },
      { value: 8 },
      { value: 9 },
      { value: 0 },
    ],
  };

  calculation = (valueToBeAdded) => {
    if (valueToBeAdded === "=") {
      try {
        let sum = eval(this.state.valueCalculate);
        this.setState({ valueCalculate: sum });
      } catch (error) {
        this.setState({valueCalculate: "error"})
      }
    }else if(valueToBeAdded === 'clear'){
      this.setState({valueCalculate : ""});
    } else {
      this.setState({
        valueCalculate: this.state.valueCalculate + valueToBeAdded,
      });
    }
  };

  render() {
    return (
      <div className="calculator">
        <CalculationBar stateToDisplay={this.state.valueCalculate} />
        <ButtonsSet
          onCalculate={this.calculation}
          onButtons={this.state.buttons}
        />
      </div>
    );
  }
}

export default Calculator;
