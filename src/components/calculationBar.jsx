import React, { Component } from 'react';
import "./calculator.css";

class CalculationBar extends Component {

    render() { 
        return (
            <div className='CalculationBar'>{this.props.stateToDisplay}</div>
        );
    }
}
export default CalculationBar;